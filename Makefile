PROG=hello

run: ${PROG}
	./${PROG}

${PROG}: ${PROG}.c
	gcc ${PROG}.c -o ${PROG}

build: ${PROG}
